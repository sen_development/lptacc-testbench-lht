﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Alien" Type="Folder">
			<Item Name="Base.lvlib" Type="Library" URL="../../baseactor/Base/Base.lvlib"/>
			<Item Name="Channel.lvlib" Type="Library" URL="../../channel/Channel.lvlib"/>
			<Item Name="Device.lvlib" Type="Library" URL="../../sen_device/Device/Device.lvlib"/>
			<Item Name="Interface.lvlib" Type="Library" URL="../../channel/Interface/Interface.lvlib"/>
			<Item Name="Pressure.lvlib" Type="Library" URL="../../sen_device/Pressure/Pressure.lvlib"/>
		</Item>
		<Item Name="Demo.vi" Type="VI" URL="../Demo.vi"/>
		<Item Name="Keller_33X_ChannelInterface.lvclass" Type="LVClass" URL="../Keller_33X_ChannelInterface.lvclass"/>
		<Item Name="Pressure_Keller_33X.lvlib" Type="Library" URL="../Pressure_Keller_33X/Pressure_Keller_33X.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="eventsource.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventsource.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="crc16.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/Keller_Pressure_LV2009/Keller X-Series RS485.llb/crc16.vi"/>
			<Item Name="F48 Init.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/Keller_Pressure_LV2009/Keller X-Series RS485.llb/F48 Init.vi"/>
			<Item Name="F73 Stable Readout.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/Keller_Pressure_LV2009/Keller X-Series RS485.llb/F73 Stable Readout.vi"/>
			<Item Name="FlowUnit.ctl" Type="VI" URL="../../channel/AIFlowInput/TypeDef/FlowUnit.ctl"/>
			<Item Name="Keller_Global.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/Keller_Pressure_LV2009/Keller_Global.vi"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Open COM.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/Keller_Pressure_LV2009/Keller X-Series RS485.llb/Open COM.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="T_Keller_action.ctl" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/Keller_Pressure_LV2009/T_Keller_action.ctl"/>
			<Item Name="T_Keller_chain.ctl" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/Keller_Pressure_LV2009/T_Keller_chain.ctl"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
