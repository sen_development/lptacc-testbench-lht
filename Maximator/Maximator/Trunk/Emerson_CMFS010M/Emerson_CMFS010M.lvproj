﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Alien" Type="Folder">
			<Item Name="Base.lvlib" Type="Library" URL="../../baseactor/Base/Base.lvlib"/>
			<Item Name="Channel.lvlib" Type="Library" URL="../../channel/Channel.lvlib"/>
			<Item Name="Device.lvlib" Type="Library" URL="../../sen_device/Device/Device.lvlib"/>
			<Item Name="Flow.lvlib" Type="Library" URL="../../sen_device/Flow/Flow.lvlib"/>
			<Item Name="Interface.lvlib" Type="Library" URL="../../channel/Interface/Interface.lvlib"/>
		</Item>
		<Item Name="Demo.vi" Type="VI" URL="../Flow_Emerson_CMFS010M/Demo.vi"/>
		<Item Name="Emerson_CMFS010MAIChannelInterface.lvclass" Type="LVClass" URL="../Emerson_CMFS010MAIChannelInterface.lvclass"/>
		<Item Name="Flow_Emerson_CMFS010M.lvlib" Type="Library" URL="../Flow_Emerson_CMFS010M/Flow_Emerson_CMFS010M.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="eventsource.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventsource.ctl"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="lveventtype.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/lveventtype.ctl"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Flush IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Flush IO Buffer Mask.ctl"/>
				<Item Name="VISA Set IO Buffer Mask.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Set IO Buffer Mask.ctl"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Change_Byte_Order.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/Change_Byte_Order.vi"/>
			<Item Name="EMERSON_check_floating_point_order.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/EMERSON_check_floating_point_order.vi"/>
			<Item Name="EMERSON_check_units.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/EMERSON_check_units.vi"/>
			<Item Name="EMERSON_config_device.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/EMERSON_config_device.vi"/>
			<Item Name="EMERSON_set_floating_point_order.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/EMERSON_set_floating_point_order.vi"/>
			<Item Name="EMERSON_set_units.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/EMERSON_set_units.vi"/>
			<Item Name="FlowUnit.ctl" Type="VI" URL="../../channel/AIFlowInput/TypeDef/FlowUnit.ctl"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="MBcrc.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/MBcrc.vi"/>
			<Item Name="MBSerialReadInstance.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/MBSerialReadInstance.vi"/>
			<Item Name="MBwaitForSerialRegisters3.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/MBwaitForSerialRegisters3.vi"/>
			<Item Name="Prepare_Data.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/Prepare_Data.vi"/>
			<Item Name="process_received_data.vi" Type="VI" URL="/C/Users/m.reichardt/Desktop/Maximator_Example_VIs_2019_05_16/Maximator_Example_VIs_2019_05_16/MicroMotion_Coriolis_LV2016/Emerson_Coriolis_Read_Out_Ver1_2.llb/process_received_data.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
